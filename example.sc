import io.circe.Json
import io.circe.syntax.*
import io.circe.yaml.syntax.*

import scala.annotation.targetName

case class Person(name: String, age: Int)

import io.circe.Encoder
import io.circe.generic.auto._
import io.circe.{Encoder, Json, JsonObject}
import io.circe.syntax._
import enumeratum.EnumEntry

import enumeratum._

sealed trait PullPolicy extends EnumEntry

object PullPolicy extends Enum[PullPolicy] {
  case object IfNotPresent extends PullPolicy
  case object Always extends PullPolicy
  case object Never extends PullPolicy
  val values = findValues
}


sealed trait AccessMode extends EnumEntry

object AccessMode extends Enum[AccessMode] {
  case object ReadWriteOnce extends AccessMode
  case object ReadOnlyMany extends AccessMode
  case object ReadWriteMany extends AccessMode
  case object ReadWriteOncePod extends AccessMode

  val values = findValues
}


sealed trait TypeOf extends EnumEntry

object TypeOf extends Enum[TypeOf] {
  case object ClusterIP extends TypeOf
  case object NodePort extends TypeOf
  case object LoadBalancer extends TypeOf
  case object ExternalName extends TypeOf
  case object Headless extends TypeOf

  val values = findValues
}


case class Image
(
  repository: String, tag: String, pullPolicy: PullPolicy
)
case class Service
(
  type_of: TypeOf, port: Int
)
case class Persistence
(
  enabled: Boolean,
  size: Int,
  accessMode: List[AccessMode]

)

case class Ingress
(
  enabled: Boolean,

)

case class Values
(
  image: Image,
  service: Service,
  enabled: Boolean,
  persistence: Persistence,
  adminUser: String,
  adminPassword: String
)
implicit val ValuesEncoder: Encoder[Values] = deriveEncoder
implicit val AccessModeEncoder: Encoder[AccessMode] = Encoder.encodeString.contramap(_.entryName)
implicit val PullPolicyEncoder: Encoder[PullPolicy] = Encoder.encodeString.contramap(_.entryName)
implicit val TypeOfEncoder: Encoder[TypeOf] = Encoder.encodeString.contramap(_.entryName)

implicit val PersistenceEncoder: Encoder[Persistence] = new Encoder[Persistence] {
  final def apply(persistence: Persistence): Json = {
    Json.obj(
      "enabled" -> Json.fromBoolean(persistence.enabled),
      "size" -> Json.fromString(s"${persistence.size}Gi"),
      "accessMode" -> persistence.accessMode.asJson
    )
  }
}

implicit val ServiceEncoder: Encoder[Service] = new Encoder[Service] {
  final def apply(service: Service): Json = {
    Json.obj(
      "type" -> service.type_of.asJson,
      "port" -> Json.fromInt(service.port)
    )
  }
}


val image = Image("grafana/grafana", "8.2.1", PullPolicy.IfNotPresent)
val service = Service(TypeOf.ClusterIP, 80)
val persistence = Persistence(true, 1, List(AccessMode.ReadWriteOnce))

val values = Values(image, service, true, persistence, "root", "root")

val yamlString = values.asJson.asYaml.spaces2
println(yamlString)