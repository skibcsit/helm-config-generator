trait ProgramAlgebra {
  type TProgram
  type TFunctionDecl
  type TFunctionImpl
  def emptyProgram(name: String): TProgram
  def addFunction(
    program: TProgram,
    decl: TFunctionDecl,
    impl: TFunctionImpl,
  ): TProgram
  def functionDecl(name: String, argNames: List[String]): TFunctionDecl
}

trait HelpInterpreter extends ProgramAlgebra {
  type TProgram = String
  type TFunctionDecl = String
  type TFunctionImpl = Unit
  def emptyProgram(name: String): String = s"\n\nProgram $name usage:"
  def addFunction(program: String, decl: String, impl: Unit): String =
    program + s"\n  <program> $decl"
  def functionDecl(name: String, argNames: List[String]): String =
    s"$name " + argNames.map(argName => s"--$argName <value>").mkString(" ")
}

trait MainInterpreter extends ProgramAlgebra {
  type TProgram = Array[String] => Option[Unit]
  type TFunctionDecl = Array[String] => Option[Array[String]]
  type TFunctionImpl = Array[String] => Unit
  def emptyProgram(name: String): TProgram = _ => false
  def addFunction(
    program: TProgram,
    decl: TFunctionDecl,
    impl: TFunctionImpl,
  ): TProgram =
    args => program(args).orElse(decl(args).foreach(impl))
  def functionDecl(name: String, argNames: List[String]): TFunctionDecl =
    args =>
      if (args.head == name) ??? // распарсить параметры
      else None
}

trait MyProgram extends ProgramAlgebra {
  def oneImpl: TFunctionImpl
  def twoImpl: TFunctionImpl
  def program =
    addFunction(
      addFunction(
        emptyProgram("myprog"),
        functionDecl("one", List("x", "y")),
        oneImpl,
      ),
      functionDecl("two", List("param")),
      twoImpl,
    )
}

object MyProgramHelp extends MyProgram with HelpInterpreter {
  val oneImpl = ()
  val twoImpl = ()
}

MyProgramHelp.program
